<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
     protected $fillable = [
    'title',
    'photo',
    'description',
    'album_id'];


      public function album()
    {
        return $this->belongsTo('App\Album');
    }
}


