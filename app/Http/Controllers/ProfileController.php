<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use auth;

class ProfileController extends Controller
{

    public function edit()
    {
        $user = auth::user();
        return view('userProfile', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   

         $this->validate($request, [
        'name' => 'required|string|min:4|max:20',
        'password' => 'min:6',
        'email'  => 'required|email|unique:users,email',
        ]);


         $data = $request->all();

         unset($data['is_admin']);
        
        if(!empty($request->password)){
            // jei password laukas nera tuscias - perrasom masyvo 'password' reiksme su jo pacio reiksme tik uzhashinta
            $data['password'] = \Hash::make($request->password);
        }else{
            // ismetam 'password' key is masyvo, nes kitaip uzsaugotu kaip tuscia password'a
            unset($data['password']);
        }
        
        // atnaujinam user duomenis su modifikuotais duomenimis
        $user = \App\User::find(auth::user()->id)->update($data);

        return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        \App\User::find(auth::user()->id)->delete();
        return redirect()->route('index');
    }
}
