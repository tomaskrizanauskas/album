<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
     public function __construct(){
        $this->middleware('admin');
        
    }


        public function index(Request $request)
    {   
        $users = \App\User::simplePaginate(10);

        return view('users', compact('users'));
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = \App\User::find($id);
        return view('userEdit', compact('user')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
         $this->validate($request, [
        'name' => 'required|string|min:4|max:20',
        'password' => 'min:6',
        'email'  => 'required|email|unique:users,email',
        'is_admin' => 'boolean'
        ]);

        $data = $request->all();
        
        if(!empty($request->password)){
            // jei password laukas nera tuscias - perrasom masyvo 'password' reiksme su jo pacio reiksme tik uzhashinta
            $data['password'] = \Hash::make($request->password);
        }else{
            // ismetam 'password' key is masyvo, nes kitaip uzsaugotu kaip tuscia password'a
            unset($data['password']);

        }
        
        // atnaujinam user duomenis su modifikuotais duomenimis
        $user = \App\User::find($id)->update($data);

        return redirect()->route('users.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\User::find($id)->delete();
        return redirect()->route('users.index');
    }
}
