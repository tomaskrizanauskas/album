<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct(){
        $this->middleware('admin', ['except' => ['show']]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function create(Request $id = null)
    {   

        $album_id = $id->id;
        $albums = \App\Album::all();
        

        $selectAlbums = [];

        foreach ($albums as $album) {
            $selectAlbums[$album->id] = $album->title;
        }
        return view('upload',compact('selectAlbums','album_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
    {   
         $this->validate($request, [
        'photo' => 'required|image',
        'title' => 'min:4|max:32|string|required',
        'album_id' => 'required|exists:albums,id|integer',
        'description' => 'required|string|min:4|max:150',

        ]);

        // if ($request->hasFile('photo') && substr($request->file('photo')->getMimeType(), 0, 5) == 'image') {
    
        $photo = $request->file('photo');

        $img = \Storage::disk('public')->put('photos', $photo);
            // $photo = true;

        // }else {
        //     $photo = false;
        // }

         $photo = \App\Photo::create(array_merge($request->all(), ['photo' => $img]));
        // $destinationPath = base_path() . '/public/uploads/dishes/';
        // $photo->move($destinationPath, $photo->getClientOriginalName());


         ////////////////xnuu

        return redirect()->route('index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $image = \App\Photo::find($id);
        $photos = \App\Photo::whereNotIn('id', [$id])->get();

        return view('photoSlide', compact('image', 'photos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $photo = \App\Photo::find($id);
        $albums = \App\Album::all();

        $selectAlbums = [];

        foreach ($albums as $album) {
            $selectAlbums[$album->id] = $album->title;
        }
        return view('upload', compact('photo', 'selectAlbums'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {    

        $this->validate($request, [
        'photo' => 'image',
        'title' => 'min:4|max:32|string|required',
        'album_id' => 'required|exists:albums,id|integer',
        'description' => 'required|string|min:4|max:150',

        ]);


        $data = $request->all();
        
        if(!$request->hasFile('photo')){
            unset($data['photo']);

        }else{

        $img = \Storage::disk('public')->put('photos', $request->file('photo'));
        $data['photo'] = $img;

        }
        // atnaujinam duomenis su modifikuotais duomenimis

        $photo = \App\Photo::find($id)->update($data);
        return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   


        $album_id = \App\Photo::find($id)->album_id;

        $filename = \App\Photo::find($id)->photo;

        Storage::delete($filename);

        \App\Photo::find($id)->delete();

        return redirect()->route('album.show',$album_id);
    }
}
