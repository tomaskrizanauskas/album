<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Auth::routes();

Route::group(['middleware' => 'auth'], function () {

	Route::get('/', 'HomeController@index')->name('index');

	Route::resource('/album', 'AlbumController');

	Route::resource('/photo', 'PhotoController');

	Route::get('/profile', 'ProfileController@edit')->name('profile');

	Route::post('/profile', 'ProfileController@update')->name('profile.update');

	Route::delete('/profile', 'ProfileController@destroy')->name('profile.destroy');
	
});


Route::resource('/users', 'UserController');




Route::get('/about', function(){
	return view('about');
})->name('about');