@extends('layouts.app')

@section('content')


<div class="container">
	<div class="row ">

		<div class="col-md-3">
			<p class="lead">Wellcome to my photo album :)</p>
			<div class="list-group">
			@if(auth::check() && auth::user()->is_admin)
				<a href="{{ route('album.create') }}" class="list-group-item">Add a new album</a>
				@endif
			@foreach ($albums as $album) 
				<a href="{{ route('album.show', $album->id) }}" class="list-group-item">{{ $album->title }}</a>

			@endforeach
			
			</div>
		</div>
		<div class="col-md-9">

			@foreach ($albums as $album)  

			<div class="row">
				<div class="blockh">
				<div class="inner">
				<h2><a href="{{ route('album.show', $album->id) }}">{{ $album->title }}</a></h2>
				@if(auth::check() && auth::user()->is_admin)
				<a href="{{ route('photo.create')}}?id={{$album->id}}">Add a new photo</a>
				<a href ="{{ route('album.edit', $album->id) }}">( Edit )</a>
				@endif
				</div>
				</div>
			</div>
			<div class="row">
				@foreach($album->photos->take(3) as $index => $photo)

				<div class="col-sm-4 col-lg-4 col-md-4">
					<div class="photo">
						<div class="photo-img">
							<a href="{{ route('photo.show', $photo->id)}}"><img class="img img-responsive" src="{{ url($photo->photo) }}"></a>
						</div>
						<div class="photo-info">
						@if(auth::check() && auth::user()->is_admin)
							<h4 class="pull-right"><a class="btn btn-default" href="{{ route('photo.edit', $photo->id)}}">Edit</a></h4>
						@endif
							<h4 class="title"><a href="{{ route('photo.show', $photo->id)}}">{{ $photo->title }}</a>
							</h4>
							<p><i>{{ mb_strimwidth($photo->description, 0, 150, '...') }}</i></p>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			@endforeach
		</div>

	</div>
	
</div>

@endsection








