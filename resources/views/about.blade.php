@extends('layouts.app')

@section('content')

<div class="container">
    <div class="jumbotron jumbotron-sm">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-xs-12 col-md-12">
                <h1 class="h2">
                    About me
                </h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-xs-12 col-md-6">
            <div class="well">
                <h3 style=""><i class="fa fa-home fa-1x"></i> Adress:</h3>               
                <p>Baltupio 45, Vilnius</p>
                <br />
                <br />
                <h3><i class="fa fa-envelope fa-1x"></i> E-Mail adress:</h3>
                <p style=>tomaskrizanauskas@gmail.com</p>
                <br />
                <br />
                <h3><i class="fa fa-user fa-1x"></i> Tomas Krizanauskas:</h3>
                <p>WEB solutions</p>
                <br />
                <br />
                <h3><i class="fa fa-yelp fa-1x"></i> WEB page:</h3>
                <p><a href="www.tomaskrizanauskas.lt">www.tomaskrizanauskas.lt</a></p>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12 col-md-6">
            <iframe width="100%" height="460" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=baltupio%2045&key=AIzaSyAgheCtCywwQtQ-CShkXzZCg-iw117FX34" allowfullscreen></iframe>
        </div>
    </div>
</div>

@endsection