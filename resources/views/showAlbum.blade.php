@extends('layouts.app')

@section('content')


<div class="container">
	<div class="row ">

		
		<h3>{{ $album_name->title }} </h3>
		@if(auth::check() && auth::user()->is_admin)
		<div class="inner pb">
			<a href="{{ route('photo.create')}}?id={{$album_name->id}}">Add a new photo</a>
			<a href ="{{ route('album.edit', $album_name->id) }}">( Edit )</a>
		</div>
		@endif
			

	</div>
	
		
	<div class="row">
		@foreach($photos as $i => $photo)

		<div class="col-sm-4 col-lg-4 col-md-4">
			<div class="photo">
				<div class="photo-img">
					<a href="{{ route('photo.show', $photo->id)}}"><img class="img img-responsive" src="{{ url($photo->photo) }}"></a>
				</div>
				<div class="photo-info">
				@if(auth::check() && auth::user()->is_admin)
					<h4 class="pull-right"><a class="btn btn-default" href="{{ route('photo.edit', $photo->id)}}">Edit</a></h4>
				@endif
					<h4 class="title"><a href="{{ route('photo.show', $photo->id)}}">{{ $photo->title }}</a>
					</h4>
					<p><i>{{ mb_strimwidth($photo->description, 0, 150, '...') }}</i></p>
				</div>
			</div>
		</div>
		@if(++$i % 3 == 0)
            </div><div class="row">
        @endif
		@endforeach
	</div>
	
</div>










