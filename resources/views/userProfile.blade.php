@extends("layouts.app")
@section("content")

<div class="container">
	
	<div class="row">

		<h3>Edit user</h3>
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		{{ Form::open(['route' => ["profile.update"], 'method' => 'POST']) }}
		<!-- {{ Form::hidden('_method', 'PUT') }} -->

		{{ Form::label('name', 'Name :')}}
		{{ Form::text('name', $user->name, ['class'=>'form-control']) }}
		{{ Form::label('email', 'Email :') }}
		{{ Form::text('email', $user->email, ['class'=>'form-control']) }}
		{{ Form::label('password', 'password :') }}
		{{ Form::text('password', '', ['class'=>'form-control']) }}
		

		{{ form::submit('Edit User', ['class'=>'btn block btn-primary  spacebtn']) }}
		{{ Form::close() }}
		


		{{ Form::open(['route' => ['profile.destroy'], 'method' => 'POST']) }}

		{{ Form::hidden('_method', 'DELETE') }}
		{{ form::submit('Unregister', ['class'=>'btn btn-danger']) }}

		{{ Form::close() }}



	</div>
</div>
@endsection