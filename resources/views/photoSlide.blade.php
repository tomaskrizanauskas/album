@extends("layouts.app")
@section("content")

<div class="container">
    
    <div class="row carousel-holder">

        <div class="col-md-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              
                <div class="carousel-inner">
                    <div class="item active">
                        <img class="slide-image" src="{{ url($image->photo) }}" alt="">
                    </div>
                    @foreach($photos as $photo)
                        <div class="item">
                            <img class="slide-image img-responsive" src="{{ url($photo->photo) }}" alt="{{ $photo->title }}">
                        </div>
                    @endforeach
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>

    </div>
</div>
@endsection