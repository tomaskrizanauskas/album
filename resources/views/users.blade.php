@extends("layouts.app")
@section("content")
<div class="container">
	
	<div class="row">
 <div class="table-responsive"> 
<table class="table">
	<thead>
		<tr>
			<th>Firstname</th>
			<th>Email</th>
			<th>Is Admin?</th>
			@if(Auth::user())
			<th>Action</th>
			@endif
		</tr>
	</thead>
	<tbody>
	@foreach($users as $user)
		<tr>
			<td>{{ $user->name }}</td> 
			<td>{{ $user->email }}</td>
			<td>{{ $user->is_admin? "Admin": "User" }}</td>
			@if(Auth::user())
			<td><a class="btn btn-warning" href="{{ route('users.edit', $user->id) }}">Edit</a></td>
			@endif
		</tr>
		@endforeach
	</tbody>
</table>
</div>
{{ $users->links() }}
</div>
</div>
@endsection