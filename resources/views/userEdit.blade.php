@extends("layouts.app")
@section("content")

<div class="container">
	
	<div class="row">

		<h3>Edit user</h3>

		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		{{ Form::open(['route' => ["users.update", $user->id], 'method' => 'POST']) }}
		{{ Form::hidden('_method', 'PUT') }}

		{{ Form::label('name', 'Name :')}}
		{{ Form::text('name', $user->name, ['class'=>'form-control']) }}
		{{ Form::label('email', 'Email :') }}
		{{ Form::text('email', $user->email, ['class'=>'form-control']) }}
		{{ Form::label('password', 'password :') }}
		{{ Form::text('password', '', ['class'=>'form-control']) }}
		{{ Form::label('is_admin', 'is admin ?') }}
		{{ Form::hidden('is_admin', '0') }}
		{{ Form::checkbox('is_admin', '1', $user->is_admin ) }}

		{{ form::submit('Edit User', ['class'=>'btn block btn-primary  spacebtn']) }}
		{{ Form::close() }}
		


		{{ Form::open(['route' => ['users.destroy', $user->id], 'method' => 'POST']) }}

		{{ Form::hidden('_method', 'DELETE') }}
		{{ form::submit('Delete', ['class'=>'btn btn-danger']) }}

		{{ Form::close() }}



	</div>
	</div>
@endsection