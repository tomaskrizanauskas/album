@extends("layouts.app")
@section("content")

<div class="container">
	
	<div class="row">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		<h3>{{ isset($album) ? 'Edit Album' : 'Add a new Album' }}</h3>

		@if(isset($album))
		{{ Form::open(['route' => ["album.update", $album->id], 'method' => 'POST'] ) }}
		{{ Form::hidden('_method', 'PUT') }}
		
		@else
		{{ Form::open(['route' => ["album.store"], 'method' => 'POST']) }}
		@endif

		{{ Form::label('title', 'Album name:')}}
		{{ Form::text('title', isset($album->title)?$album->title:'', ['class'=>'form-control']) }}

		{{ form::submit(isset($album) ? 'Edit Album' : 'Add new album', ['class'=>'btn btn-primary spacebtn']) }}
		{{ Form::close() }}

		@if(isset($album))
		{{ Form::open(['route' => ['album.destroy', $album->id], 'method' => 'POST']) }}

		{{ Form::hidden('_method', 'DELETE') }}
		{{ form::submit('Delete', ['class'=>'btn btn-danger ']) }}

		{{ Form::close() }}
		@endif

	</div>
</div>
@endsection