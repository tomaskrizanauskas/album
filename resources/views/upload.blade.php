@extends("layouts.app")
@section("content")

<div class="container">
	
	<div class="row">
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		<h3>{{ isset($photo) ? 'Edit Photo' : 'Add a new photo' }}</h3>

		@if(isset($photo))
		{{ Form::open(['route' => ["photo.update", $photo->id], 'method' => 'POST', 'files' => true] ) }}
		{{ Form::hidden('_method', 'PUT') }}
		
		@else
		{{ Form::open(['route' => ["photo.store"], 'method' => 'POST', 'files' => true]) }}
		@endif

		{{ Form::label('title', 'Photo title:')}}
		{{ Form::text('title', isset($photo->title)?$photo->title:'', ['class'=>'form-control']) }}

		{{ Form::label('title', 'Paveiksliukas:')}}
		{{ Form::file('photo', ['class'=>'form-control']) }}


		{{ Form::label('album', 'Album:')}}

		
		{{Form::select('album_id', $selectAlbums, isset($photo->album_id)?$photo->album_id:$album_id, ['class'=>'form-control']) }}
		


		{{ Form::label('description', 'Photo description:')}}
		{{ Form::textarea('description',isset($photo->description)?$photo->description:'', ['class'=>'form-control', 'rows' => '10', 'cols' => '30']) }}

		{{ form::submit(isset($photo) ? 'Edit Photo' : 'Add new photo', ['class'=>'btn btn-primary spacebtn']) }}
		{{ Form::close() }}

		@if(isset($photo))
		{{ Form::open(['route' => ['photo.destroy', $photo->id], 'method' => 'POST']) }}

		{{ Form::hidden('_method', 'DELETE') }}
		{{ form::submit('Delete', ['class'=>'btn btn-danger']) }}

		{{ Form::close() }}
		@endif

	</div>
</div>
@endsection